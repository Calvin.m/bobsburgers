package com.rave.bobsburgers.model.remote

import com.rave.bobsburgers.model.remote.dtos.BobCharacterDTO
import com.rave.bobsburgers.model.remote.dtos.CharacterDTO
import com.rave.bobsburgers.model.remote.dtos.CharacterResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CharacterService {

    companion object {
        const val BASE_URL = "https://bobsburgers-api.herokuapp.com/"
        private const val CHARACTER_ENDPOINT = "characters"
        private const val SORT_QUERY = "sortBy"
        private const val ORDER_QUERY = "OrderBy"
        private const val LIMIT_QUERY = "limit"
        private const val SKIP_QUERY = "skip"
    }

    @GET(CHARACTER_ENDPOINT)
    fun getCharacters(
        @Query(LIMIT_QUERY)charLim: Int,
        @Query(SORT_QUERY)sort: String = "name",
        @Query(ORDER_QUERY)order: String = "asc",
        @Query(SKIP_QUERY)skip: Int = 0
    ): Call<List<BobCharacterDTO>>
}
