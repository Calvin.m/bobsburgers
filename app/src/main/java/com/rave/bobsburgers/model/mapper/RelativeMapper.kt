package com.rave.bobsburgers.model.mapper

import com.rave.bobsburgers.model.local.Relative
import com.rave.bobsburgers.model.remote.dtos.RelativeDTO

class RelativeMapper : Mapper<RelativeDTO, Relative> {
    override fun invoke(dto: RelativeDTO): Relative = with(dto) {
        Relative(
            name,
            relationship,
            url,
            wikiUrl
        )
    }
}