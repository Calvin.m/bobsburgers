package com.rave.bobsburgers.model.remote.dtos

import com.rave.bobsburgers.model.local.Relative
import com.rave.bobsburgers.model.mapper.RelativeMapper

data class RelativeDTO(
    val name: String = "",
    val relationship: String = "",
    val url: String = "",
    val wikiUrl: String = ""
) {
    fun toRelative(mapper: RelativeMapper): Relative {
        return mapper(this)
    }
}