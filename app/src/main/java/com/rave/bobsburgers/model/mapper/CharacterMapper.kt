package com.rave.bobsburgers.model.mapper

import com.rave.bobsburgers.model.local.BobCharacter
import com.rave.bobsburgers.model.remote.dtos.BobCharacterDTO

class CharacterMapper : Mapper<BobCharacterDTO, BobCharacter> {
    private val relativeMapper by lazy { RelativeMapper() }

    override fun invoke(dto: BobCharacterDTO): BobCharacter =with(dto) {
        val relative = relatives.map { it.toRelative(relativeMapper) }
        BobCharacter(
            age,
            firstEpisode,
            gender,
            hairColor,
            id,
            image,
            name,
            occupation,
            relative,
            url,
            voicedBy,
            wikiUrl
        )
    }
}
