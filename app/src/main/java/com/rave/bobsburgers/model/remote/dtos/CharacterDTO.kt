package com.rave.bobsburgers.model.remote.dtos

data class CharacterDTO(
    val age: String = "",
    val firstEpisode: String = "",
    val gender: String = "",
    val hairColor: String = "",
    val id: Int = 0,
    val image: String = "",
    val name: String = "",
    val occupation: String = "",
    val relatives: List<RelativeDTO> = emptyList(),
    val url: String = "",
    val voicedBy: String = "",
    val wikiUrl: String = ""
)