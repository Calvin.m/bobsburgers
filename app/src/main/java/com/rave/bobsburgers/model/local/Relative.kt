package com.rave.bobsburgers.model.local

data class Relative(
    val name: String,
    val relationship: String,
    val url: String,
    val wikiUrl: String
)
