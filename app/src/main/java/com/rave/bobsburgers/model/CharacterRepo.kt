package com.rave.bobsburgers.model

import com.rave.bobsburgers.model.mapper.CharacterMapper
import com.rave.bobsburgers.model.remote.CharacterService
import com.rave.bobsburgers.model.remote.NetworkResponse
import com.rave.bobsburgers.model.remote.dtos.CharacterResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
// Todo: ask why this is never imported correctly
import com.rave.bobsburgers.model.local.BobCharacter
import com.rave.bobsburgers.model.remote.dtos.BobCharacterDTO

class CharacterRepo(
    private val service: CharacterService,
    private val mapper: CharacterMapper
) {
    suspend fun getCharacters(limit: Int): NetworkResponse<*> = withContext(Dispatchers.IO) {
        val characterResponse: Response<List<BobCharacterDTO>> =
            service.getCharacters(limit).execute()

        return@withContext if (characterResponse.isSuccessful) {
            val list = characterResponse.body() ?: emptyList()
            val result = list.map { mapper(it) }
            NetworkResponse.SuccessfulCharacter(result)
        } else {
            NetworkResponse.Error(characterResponse.message())
        }
    }
}
