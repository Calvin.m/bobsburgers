package com.rave.bobsburgers.view.characterscreen

import com.rave.bobsburgers.model.local.BobCharacter

data class CharacterScreenState(
    val isLoading: Boolean = false,
    val characters: List<BobCharacter> = emptyList(),
    val error: String = "",
    val isSelected: Boolean = false,
    val selectedCharacterId: Int? = null
)
