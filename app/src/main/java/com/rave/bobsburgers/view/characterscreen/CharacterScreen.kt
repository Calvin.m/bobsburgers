package com.rave.bobsburgers.view.characterscreen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
//todo: need to manually import again

@Composable
fun CharacterScreen(
    state: CharacterScreenState,
    selectCharacterClick: (Int) -> Unit) {
    LazyColumn() {
        items(state.characters) { character ->
            Column (
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Card(modifier = Modifier.clickable{selectCharacterClick(character.id)}) {
                    AsyncImage(
                        model = character.image,
                        contentDescription = "an image",
                        modifier = Modifier.size(200.dp, 200.dp)
                    )
                    Text(text = character.name)
                }
            }
        }
    }
}
