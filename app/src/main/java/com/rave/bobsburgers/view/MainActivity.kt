package com.rave.bobsburgers.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.rave.bobsburgers.model.CharacterRepo
import com.rave.bobsburgers.model.mapper.CharacterMapper
import com.rave.bobsburgers.model.remote.RetrofitClass
import com.rave.bobsburgers.ui.theme.BobsBurgersTheme
import com.rave.bobsburgers.view.characterscreen.CharacterDetailScreen
import com.rave.bobsburgers.view.characterscreen.CharacterScreen
import com.rave.bobsburgers.viewmodel.CharacterViewModel
import com.rave.bobsburgers.viewmodel.VMFactory

class MainActivity : ComponentActivity() {
    private val characterViewModel by viewModels<CharacterViewModel> {
        val service = RetrofitClass.getBurgerService()
        val repo = CharacterRepo(service, CharacterMapper())
        VMFactory(repo)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        characterViewModel.getCharacters()
        setContent {
            val characterState by characterViewModel.characterState.collectAsState()
            BobsBurgersTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {
                    if (characterState.isLoading) {
                        Column(
                            horizontalAlignment = Alignment.CenterHorizontally,
                            verticalArrangement = Arrangement.Center
                        ) {
                            CircularProgressIndicator()
                        }
                    } else {
                        if (!characterState.isSelected) {
                            //                      params you take in -> what is returned
                            CharacterScreen(characterState,
                                { id: Int -> characterViewModel.selectedCharacter(id) })
                        } else {
                            val selectedCharacter =
                                characterState.characters.find { characterState.selectedCharacterId == it.id }
                                    ?: characterState.characters[0]
                            CharacterDetailScreen(selectedCharacter)
                        }
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    BobsBurgersTheme {
        Text(text = "test")
    }
}
