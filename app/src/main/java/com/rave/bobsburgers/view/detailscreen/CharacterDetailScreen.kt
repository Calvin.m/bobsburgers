package com.rave.bobsburgers.view.characterscreen

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.rave.bobsburgers.model.local.BobCharacter

@Composable
fun CharacterDetailScreen(char: BobCharacter) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Card(modifier = Modifier.padding(12.dp)) {
            Box(modifier = Modifier.padding(12.dp)) {
                Column() {
                    Row() {
                        Column(
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Text(text = "${char.name.uppercase()}")
                        }
                        Column() {
                            AsyncImage(
                                model = char.image,
                                contentDescription = null,
                                modifier = Modifier.size(200.dp, 200.dp)
                            )
                        }
                    }
                    Text(text = "Name: ${char.name}")
                    Text(text = "url: ${char.url}")
                    Text(text = "First episode: ${char.firstEpisode}")
                    Text(text = "Gender: ${char.gender}")
                    Text(text = "Hair Color: ${char.hairColor}")
                }
//                Button(onClick = { }) {
//                    Text(text = "Character List")
//                }
            }
        }
    }
}