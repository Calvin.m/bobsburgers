package com.rave.bobsburgers.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.rave.bobsburgers.model.CharacterRepo
import com.rave.bobsburgers.model.remote.NetworkResponse
import com.rave.bobsburgers.view.characterscreen.CharacterScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class CharacterViewModel(
    private val repo: CharacterRepo
) : ViewModel() {
    private val TAG = "CharacterViewModel"

    private val _characters: MutableStateFlow<CharacterScreenState> =
        MutableStateFlow(CharacterScreenState())
    val characterState: StateFlow<CharacterScreenState> get() = _characters

    fun getCharacters(limit: Int = 30) = viewModelScope.launch {
        _characters.update { it.copy(isLoading = true) }
        when (val characters = repo.getCharacters(limit)) {
            is NetworkResponse.Error -> {
                _characters.update {
                    it.copy(
                        isLoading = false,
                        error = characters.message
                    )
                }
            }
            is NetworkResponse.SuccessfulCharacter -> {
                _characters.update { it.copy(isLoading = false, characters = characters.characters) }

            }
            else -> {
                Log.e(TAG, "ELSE!")
            }
        }
    }

    fun selectedCharacter(id:Int) {
        _characters.update { it.copy(isSelected = true, selectedCharacterId = id)
        }
    }
}

class VMFactory(
    private val repo: CharacterRepo
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CharacterViewModel::class.java)) {
            return CharacterViewModel(repo) as T
        } else {
            throw IllegalArgumentException("Illegal Arg Exception!")
        }
    }
}